FROM adoptopenjdk/openjdk11:alpine-jre
RUN apk add tzdata
ADD target/order-service.jar order-service.jar
RUN mkdir -p /applogs
VOLUME ["/applogs"]
CMD [ "java", "-jar", "/order-service.jar" ]