package net.acardenas.posadev2020.orderservice.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Item {

  @NotEmpty
  private String sku;
  @NotEmpty
  private Double qty;
  private String coupon;

}
