package net.acardenas.posadev2020.orderservice.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Data
@NoArgsConstructor
@Document(collection = "orders")
public class Order {

  @Id
  private String id;
  @NotEmpty
  private Set<Item> items;

}
