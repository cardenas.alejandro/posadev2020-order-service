package net.acardenas.posadev2020.orderservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.acardenas.posadev2020.orderservice.domain.Item;
import net.acardenas.posadev2020.orderservice.domain.Order;
import net.acardenas.posadev2020.orderservice.exception.NotFoundException;
import net.acardenas.posadev2020.orderservice.exception.OutOfStockException;
import net.acardenas.posadev2020.orderservice.repository.OrderRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedList;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderService {

  private final OrderRepository orderRepository;
  private final RestTemplate restTemplate;

  public Order create(Order order) {
    try {
      log.info("Create Order = {}", order);
      LinkedList<Item> outOfStock = new LinkedList<>();
      boolean canContinue = order.getItems().stream()
        .allMatch(item -> {
          ResponseEntity<Double> response = restTemplate
            .getForEntity("http://inventory-service/{id}", Double.class, item.getSku());
          log.info("Inventory Response : {} ", response.getStatusCodeValue());

          if (response.getStatusCode().is4xxClientError()) {
            log.error("Retrieve Inventory to sku {}, Fails with code: {}", item.getSku(),
              response.getStatusCode());
          }

          Double availableInventory = response.getBody();
          log.info("Inventory Response = {}", availableInventory);

          if (item.getQty() > availableInventory) {
            outOfStock.add(item);
          }
          return availableInventory >= item.getQty();
        });

      if (!canContinue) {
        log.error("Items Out Of Stock Required are = {}", outOfStock);
        throw new OutOfStockException("Items out of Stock : " + outOfStock);
      }

      return orderRepository.save(order);
    } catch (RestClientException e) {
      log.error("Retrieve Inventory Not Found", e);
      throw e;
    }
  }

  public Order getOrder(String id) {
    log.info("Finding Order with id : {}", id);
    return orderRepository.findById(id).orElseThrow(NotFoundException::new);
  }
}
