package net.acardenas.posadev2020.orderservice.repository;

import net.acardenas.posadev2020.orderservice.domain.Order;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OrderRepository extends MongoRepository<Order, String> {

}
