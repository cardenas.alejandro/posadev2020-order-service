package net.acardenas.posadev2020.orderservice.controller;

import lombok.extern.slf4j.Slf4j;
import net.acardenas.posadev2020.orderservice.domain.Item;
import net.acardenas.posadev2020.orderservice.domain.Order;
import net.acardenas.posadev2020.orderservice.exception.OutOfStockException;
import net.acardenas.posadev2020.orderservice.service.OrderService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.LinkedList;

@Slf4j
@RestController
@RequestMapping("/")
public class OrderController {

  private final OrderService orderService;
  private final RestTemplate restTemplate;

  public OrderController(OrderService orderService,
    RestTemplate restTemplate) {
    this.orderService = orderService;
    this.restTemplate = restTemplate;
  }

  @GetMapping("/{id}")
  public ResponseEntity<Order> getOrder(@PathVariable String id) {
    log.info("getOrder : {}", id);
    return ResponseEntity.ok(orderService.getOrder(id));
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity createOrder(@Valid @RequestBody Order order) {

    log.info("Creating Order with values : {}", order);

    log.info("Saving Order");
    Order savedOrder = orderService.create(order);
    log.info("Order Id is: {}", savedOrder.getId());

    URI location = ServletUriComponentsBuilder.fromCurrentRequest()
      .path("/{id}")
      .buildAndExpand(savedOrder.getId())
      .toUri();

    return ResponseEntity.created(location).build();
  }

}
